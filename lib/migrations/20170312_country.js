// NOTE: each entity here is related to the current server, since MonsterDbms'll always run locally

exports.up = function(knex, Promise) {

    return Promise.all([

         knex.schema.alterTable('eventlog', function(table) {
            table.string('e_country').notNullable().defaultTo("");

        }),

    ])
    
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.alterTable('eventlog', function(table){
            table.dropColumn("e_country");
        }),        
    ])
    
};
