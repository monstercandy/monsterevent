var lib = module.exports = function(knex, vali, app) {


    function eventlogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["l_username","l_event_type","l_subsystem", "l_timestamp", "l_ip"], default: "l_timestamp"},
               "desc": {isBooleanLazy: true},

               "result_apply_timezone": {isString: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},

               "l_event_type": {isString: true},
               "l_event_type_like": {isString: {trim:true}},
               "l_other": {isString: true},
               "l_subsystem": {isString: true},
               "l_username": {isString: true},
               "l_agent": {isString: true},
               "l_user_id": {isString: true},
               "l_ip": {isString: {trim:true}},
               "l_subsystem": {isString: {trim:true}},
               "l_processed": {isBooleanLazy:true},
          })
    }
    function eventlogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("l_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("l_timestamp<=?")
          qArr.push(d.notafter)
       }

       if(d.l_other){
          qStr.push("l_other LIKE ?")
          qArr.push('%'+d.l_other+'%')
       }
       if(d.l_username){
          qStr.push("l_username LIKE ?")
          qArr.push('%'+d.l_username+'%')
       }
       if(d.l_user_id){
          qStr.push("l_user_id=?")
          qArr.push(d.l_user_id)
       }
       if(d.l_ip){
          qStr.push("l_ip LIKE ?")
          qArr.push('%'+d.l_ip+'%')
       }
       if(d.l_agent){
          qStr.push("l_agent LIKE ?")
          qArr.push('%'+d.l_agent+'%')
       }
       if(d.l_event_type_like){
          qStr.push("l_event_type LIKE ?")
          qArr.push(d.l_event_type_like+'%')
       }
       if(d.l_event_type){
          qStr.push("l_event_type=?")
          qArr.push(d.l_event_type)
       }
       if(d.l_subsystem){
          qStr.push("l_subsystem=?")
          qArr.push(d.l_subsystem)
       }
       if(typeof d.l_processed != "undefined"){
          qStr.push("l_processed=?")
          qArr.push(d.l_processed)
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)

    }



    var re = {}


    const moment = require("MonsterMoment") // we require this so now will exist for sure
    const MError = app.MError



    re.Query = function (pd) {
           var d
           return eventlogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = eventlogQueryWhere(d,knex.select()
                   .table('loginlog')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                moment.ApplyTimezoneOnArrayOfHashes(rows, ["l_ts"], d.result_apply_timezone)
                return Promise.resolve({"events": rows })

              })
    }

    re.Count = function (d) {
           return eventlogQueryParams(d)
              .then(d=>{
                 var s = eventlogQueryWhere(d, knex('loginlog').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }


    re.ReportLoginEvents = function(){
         var ids;
         var limit = app.config.get("max_number_of_events_in_a_batch_report");
         return eventlogQueryWhere({l_processed:0}, knex('loginlog').select().limit(limit))
           .then(rows=>{
              if(rows.length <= 0) return;

              console.log("reporting login events");
              ids = rows.map(x => "'"+x.l_id+"'");

              rows.map(function(x){
                 delete x.l_processed;
                 return x;
              })

              return app.GetRelayerMapiPool().putAsync("/accountapi/loginlog/events", {events:rows})
               .then(()=>{
                  return knex("loginlog")
                    .update({l_processed: 1})
                    .whereRaw("l_id IN ("+ids.join(',')+")", [])
                    .return({reportedCount:ids.length})

               })
               .then(()=>{
                  if(rows.length >= limit)
                      return re.ReportLoginEvents();
               })

           })

    }


    installCron();

    return re;

    function installCron(){
       if(app.loginLogReporter) return;
       app.loginLogReporter = true;

       var croner = require("Croner");
       var csp = app.config.get("cron_loginlog_report");
       croner.schedule(csp, function(){
          return re.ReportLoginEvents();
       });

    }

}

