// permissions needed: ["LOGINLOG_EVENT_REPORT","BAN_IP","ESCALATE_EXCEPTION", "FEED_HOSTSSH_IP", ?"INFO_GEOIP"? ]
module.exports = function(moduleOptions) {
      require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

      const path = require('path')
      const util = require('util');
      var fs = require("fs");

      var config = require('MonsterConfig');
      var me = require('MonsterExpress');
      const MError = me.Error

      const moment = require("MonsterMoment")


      config.defaults({
         skip_slowlog_when_network_related: [
           "gethostbyaddr()", 
           "curl_exec()",
           "backup_files()",
           "download_json",
           
           ],
         dont_escalate_exceptions:["VALIDATION_ERROR","AUTH_FAILED", "INVALID_TOKEN_OR_SESSION_EXPIRED","TOTP_REQUIRED"],

         cron_loginlog_report: "*/1 * * * *",
         max_number_of_events_in_a_batch_report: 100,

         mail_subsystem_ssh_whitelister_emails: [],

         event_sync_flush_seconds: 5,
         exception_flush_seconds: 5,
         exception_flush_seconds_failure: 5*60,

         ban_after_unsuccessful_attempts_count_all: 10,
         ban_after_unsuccessful_attempts_in_seconds: 60*60,
         ban_release_logic_interval_second: 60,

         subsystem_to_ban_category: {
            "ftp": "FTP",
            "email": "EMAIL",
            "accountapi": "HTTP",
         },

         cleanup: {
            "0 5 * * *": [
              {table: "eventlog", agePrefix: "e_", ageDays: 14, rawFilter:"e_event_type LIKE 'auth-%' AND e_subsystem!='accountapi'"},
              {table: "eventlog", agePrefix: "e_", ageDays: 90, rawFilter:"e_event_type='exception'"},
              {table: "loginlog", agePrefix: "l_", ageDays: 14, rawFilter:"l_successful=0 AND l_subsystem!='accountapi'"},
            ]
         },

      })


      config.appRestPrefix = "/eventapi"


      var app = me.Express(config, moduleOptions);
      // we dont setup subsystem here, since it would trigger event logging inside the event service and we dont want that
      var router = app.PromiseRouter(config.appRestPrefix)

      var knex

      var vali = require("MonsterValidators").ValidateJs()
      var EventLib = require("event-lib.js")

      var loginLog
      const MonsterInfoLib = require("MonsterInfo")


      app.ServersGeoip = router.ServersMiddleware("info-geoip", 0)
      app.MonsterInfoGeoip = MonsterInfoLib.geoip({config: app.config, geoipServerRouter: app.ServersGeoip})

      // or we could just use something like this:
      // app.MonsterInfoGeoip = MonsterInfoLib.geoip({config: app.config, geoipServerRouter: app.ServersRelayer})



      router.get("/config/subsystems", function(req,res,next){
         req.result = EventLib.SupportedSubSystems
         next()
      })
      router.get("/config/eventtypes", function(req,res,next){
         req.result = EventLib.SupportedEventTypes
         next()
      })

      function finishByPromise(callback){
        return function(req,res,next) {
            var eventLog = new EventLib(knex, vali, app)

            return req.sendPromResultAsIs(callback(eventLog, req))
        }
      }



      router.put("/events", finishByPromise((eventLog,req)=>{
         return eventLog.InsertByHash(req.body.json, req.query.ts)
      }))

      router.post("/events/count", finishByPromise((eventLog,req)=>{
         return eventLog.Count(req.body.json)
      }))

      router.post("/events/search", finishByPromise((eventLog,req)=>{
         return eventLog.Query(req.body.json)
      }))


      router.post("/loginlog/count", function(req,res,next){
         return req.sendPromResultAsIs(loginLog.Count(req.body.json))
      })
      router.post("/loginlog/search", function(req,res,next){
         return req.sendPromResultAsIs(loginLog.Query(req.body.json))
      })
      router.post("/loginlog/report", function(req,res,next){
         return req.sendOk(loginLog.ReportLoginEvents())
      })

      app.Prepare = function() {
         var knexLib = require("MonsterKnex");
         knex = knexLib(config);
         loginLog = require("loginlog-lib.js")(knex, vali, app);

         return knex.migrate.latest()
           .then(()=>{
              knexLib.installCleanup(knex, app.config.get("cleanup"));
           })
      }

      app.Cleanup = function() {

         config.saveVolume({"api_secret": config.getApiSecret()})

         var fn = config.get("db").connection.filename
         if(!fn) return Promise.resolve()

         return new Promise(function(suc,rej){
            fs.unlink(fn, (err)=>{
                // the database might not exists, which is not an error in this case
                //if(err) return rej(err)

                suc()
            });
         })
      }



      return app

}
