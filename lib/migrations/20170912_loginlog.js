
exports.up = function(knex, Promise) {

    return Promise.all([

        knex.schema.createTable('loginlog', function(table){
            table.uuid('l_id');
            table.uuid('l_user_id').notNullable();
            table.uuid('l_username').notNullable();
            table.timestamp('l_timestamp').notNullable();
            table.string('l_ip').notNullable();
            table.string('l_subsystem').notNullable();
            table.string('l_event_type').notNullable();
            table.string('l_agent').notNullable().defaultTo("");
            table.boolean('l_successful').notNullable();
            table.string('l_other').notNullable().defaultTo("");
            table.boolean('l_processed').notNullable().defaultTo(0);

            // todo: reverse dns, country/city
            table.string('l_country').notNullable().defaultTo("");

            table.index(["l_id"]);
            table.index(["l_user_id"]);
            table.index(["l_processed"]);
        })
        .then(()=>{
            return knex.raw(`INSERT INTO loginlog
                (l_processed, l_id, l_user_id, l_username, l_timestamp, l_ip, l_subsystem, l_event_type, l_agent, l_successful, l_other, l_country)
                SELECT 1, e_id, e_user_id, e_username, e_timestamp, e_ip, e_subsystem, e_event_type, e_agent, e_successful, '{}', e_country FROM eventlog
                `)
        })
        .then(()=>{
             return knex.schema.createTable('eventlog_new', function(table){
                    table.uuid('e_id');
                    table.uuid('e_user_id').notNullable();
                    table.uuid('e_username').notNullable();
                    table.timestamp('e_timestamp').notNullable();
                    table.string('e_ip').notNullable();
                    table.string('e_subsystem').notNullable();
                    table.string('e_event_type').notNullable();
                    table.string('e_agent').notNullable().defaultTo("");
                    table.string('e_other').notNullable().defaultTo("");
                    table.string('e_country').notNullable().defaultTo("");

                    table.index(["e_id"]);
                })

        })
        .then(()=>{
            return knex.raw(`INSERT INTO eventlog_new (e_id,e_user_id,e_username,e_timestamp,e_ip,e_agent,e_subsystem,e_event_type,e_other,e_country)
                      SELECT e_id,e_user_id,e_username,e_timestamp,e_ip,e_agent,e_subsystem,e_event_type,e_other,e_country FROM eventlog`)
        })
        .then(()=>{
            return knex.raw("DROP TABLE eventlog")
        })
        .then(()=>{
            return knex.raw("ALTER TABLE eventlog_new RENAME TO eventlog")
        }),





    ])

};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('loginlog'),
    ])

};
