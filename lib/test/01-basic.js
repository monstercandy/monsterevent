require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../event-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){

    var userId = "x"

    var nonAuthEvent = {e_user_id:userId,e_username:"foobar@foo.com",e_other:{"foo":"bar"},e_ip:"::ffff:127.0.0.1",e_agent:"Firefox",e_event_type:"data-import"};
    var event = nonAuthEvent;

    var authEvent = {e_user_id:userId,e_username:"foobar@foo.com",e_subsystem:"accountapi",e_other:{successful: true, "foo":"bar"},e_ip:"::ffff:127.0.0.1",e_agent:"Firefox",e_event_type:"auth-credential"};

    app.MonsterInfoGeoip = {
        GetInfo: function(ip) {
            assert.equal(ip, "::ffff:127.0.0.1")
            return Promise.resolve({country:"hu"})
        }
    }


    describe('eventlog tests', function() {
        it('number of eventlog entries (should be zero)', function(done) {

             mapi.post( "/events/count", {"e_user_id": userId}, function(err, result, httpResponse){

                assert.isNull(err)
                assert.equal(httpResponse.statusCode, 200)
                assert.propertyVal(result, "count", 0 )
                done()

             })
        })

        it('inserting event (should be rejected without subsystem)', function(done) {
             mapi.put( "/events", event, function(err, result, httpResponse){
                if(err) return done()

                done(new Error("should have been rejected"))
             })
        })

        it('inserting event (should be ok)', function(done) {
            event.e_subsystem="accountapi"
             mapi.put( "/events", event, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })

        it('inserting event again (should be ok)', function(done) {
             mapi.put( "/events", event, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })

        it('number of eventlog entries (should be 2)', function(done) {

             setTimeout(function(){
                 mapi.post( "/events/count", {"e_user_id": userId}, function(err, result, httpResponse){


                    assert.isNull(err)
                    assert.equal(httpResponse.statusCode, 200)
                    assert.propertyVal(result, "count", 2 )
                    done()

                 });

             }, 500)

        })

        it('inserting event with a string other (should be ok)', function(done) {
             var d = simpleCloneObject(event)
             d.e_other="foobar"
             mapi.put( "/events", d, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })

        it('eventlog entries', function(done) {

             setTimeout(function(){

                 mapi.post( "/events/search", {"e_user_id": userId}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.equal(httpResponse.statusCode, 200)
                    assert.property(result, "events")
                    assert.equal(Array.isArray(result.events), true)
                    assert.equal(result.events.length, 3)
                    assert.propertyVal(result.events[0], "e_ip", '::ffff:127.0.0.1')
                    assert.propertyVal(result.events[0], "e_other", '{"foo":"bar"}')

                    assert.propertyVal(result.events[2], "e_other", '{"data":"foobar"}')
                    done()

                 })
             }, 500);
        })


        it('inserting a login event should automatically populate the loginlog table', function(done) {
             event.e_successful = false
             mapi.put( "/events", authEvent, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })

        it('eventlog entries should be one more', function(done) {

             setTimeout(function(){

                 mapi.post( "/events/search", {"e_user_id": userId}, function(err, result, httpResponse){
                    assert.isNull(err)
                    assert.property(result, "events")
                    assert.equal(Array.isArray(result.events), true)
                    assert.equal(result.events.length, 4)
                    done()

                 })
             }, 500);
        })

        it('and there should be one unprocessed loginlog entry', function(done) {

             mapi.post( "/loginlog/search", {"l_processed": 0}, function(err, result, httpResponse){
                // console.log(err, result)
                assert.isNull(err)
                assert.property(result, "events")
                assert.equal(Array.isArray(result.events), true)
                assert.equal(result.events.length, 1)
                assert.equal(result.events[0].l_event_type, authEvent.e_event_type);
                assert.equal(result.events[0].l_successful, 1);
                done()

             })
        })


        it("getting the loginlog events reported", function(done){
            var oGetRelayerMapiPool = app.GetRelayerMapiPool
            var wasCalled = 0;
            app.GetRelayerMapiPool = function(){
                return {
                    putAsync: function(url, data){
                        //  console.log(data);
                        assert.property(data, "events");
                        var data = data.events;
                        assert.equal(url, "/accountapi/loginlog/events");
                        Array("l_timestamp","l_id").forEach(x=>{
                            assert.ok(data[0][x])
                            delete data[0][x];
                        })
                        assert.deepEqual(data, [ { l_user_id: 'x',
    l_username: 'foobar@foo.com',
    l_ip: '::ffff:127.0.0.1',
    l_subsystem: 'accountapi',
    l_event_type: 'auth-credential',
    l_agent: 'Firefox',
    l_successful: 1,
    l_other: '{"foo":"bar"}',
    l_country: 'hu' } ]);
                        wasCalled++;
                        return Promise.resolve();
                    }
                }
            }

             mapi.post( "/loginlog/report", {}, function(err, result, httpResponse){
                // console.log(err, result)
                assert.isNull(err);
                assert.equal(wasCalled, 1);
                done()

             })
        })


        it('and there should be zero unprocessed loginlog entries', function(done) {

             mapi.post( "/loginlog/count", {"l_processed": 0}, function(err, result, httpResponse){
                // console.log(err, result)
                assert.isNull(err)
                assert.deepEqual(result, {"count":0})
                done()

             })
        })


        it('inserting event with an array should be ok', function(done) {
             var d = simpleCloneObject(event)
             d.e_other=["foobar"];
             mapi.put( "/events", d, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })


   })




}, 10000)

