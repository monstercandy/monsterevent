require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../event-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){


    var exceptionEvent = {"e_event_type":"exception","e_ip":"::ffff:127.0.0.1","e_successful":false,"e_agent":"","e_other":"SOME_ERROR","e_subsystem":"relayer"}


    describe('inserting thousands of exception events', function() {

        const max_stuffs = 200;

        batchInsert(1)
        batchInsert(10)
        batchInsert(max_stuffs)

        it('requests should be served after', function(done) {

                 mapi.post( "/events/count", {}, function(err, result, httpResponse){

                    assert.isNull(err)
                    assert.equal(httpResponse.statusCode, 200)
                    assert.property(result, "count")
                    done()

                 })
        })
        it('requests should be served after', function(done) {

            setTimeout(function(){
                 mapi.post( "/events/count", {}, function(err, result, httpResponse){

                    assert.isNull(err)
                    assert.equal(httpResponse.statusCode, 200)
                    assert.property(result, "count")
                    assert.ok(result.count > max_stuffs)
                    done()

                 })

            }, 1000)

        })

        it('inserting curl_exec slowlog event', function(done) {
             var event = {"e_event_type":"exception","e_ip":"::ffff:127.0.0.1","e_successful":false,"e_agent":"","e_other":{message:"WEBHOSTING_SLOWLOG_EVENT",slowQuery:{
"sl_dump": [
                    "[0x00007f4ed941d530] curl_exec() /dulate.hu/pages/wp-content/plugins/updraftplus/includes/Google/IO/Curl.php:110",
                    "[0x00007f4ed941ce90] executeRequest() /dulate.hu/pages/wp-content/plugins/updraftplus/includes/Google/IO/Abstract.php:136",
                    "[0x00007f4ed941cbf0] makeRequest() /dulate.hu/pages/wp-content/plugins/updraftplus/includes/google-extensions.php:185",
                    "[0x00007f4ed941c790] nextChunk() /dulate.hu/pages/wp-content/plugins/updraftplus/methods/googledrive.php:957",
                    "[0x00007f4ed941bb00] upload_file() /dulate.hu/pages/wp-content/plugins/updraftplus/methods/googledrive.php:508",
                    "[0x00007f4ed941afa0] backup() /dulate.hu/pages/wp-content/plugins/updraftplus/backup.php:489",
                    "[0x00007f4ed941ac10] upload_cloud() /dulate.hu/pages/wp-content/plugins/updraftplus/backup.php:438",
                    "[0x00007f4ed941a130] cloud_backup() /dulate.hu/pages/wp-content/plugins/updraftplus/class-updraftplus.php:2343",
                    "[0x00007f4ed9417070] backup_resume() /dulate.hu/pages/wp-content/plugins/updraftplus/class-updraftplus.php:2774",
                    "[0x00007f4ed9415c20] boot_backup() /dulate.hu/pages/wp-content/plugins/updraftplus/class-updraftplus.php:2496",
                    "[0x00007f4ed9415bb0] backup_files() /dulate.hu/pages/wp-includes/class-wp-hook.php:286",
                    "[0x00007f4ed9415820] apply_filters() /dulate.hu/pages/wp-includes/class-wp-hook.php:310",
                    "[0x00007f4ed9415760] do_action() /dulate.hu/pages/wp-includes/plugin.php:515",
                    "[0x00007f4ed9415560] do_action_ref_array() /dulate.hu/pages/wp-cron.php:126",
                    ""
                ],

             }},"e_subsystem":"relayer"}
             mapi.put( "/events", event, function(err, result, httpResponse){
                assert.equal(result,"ok")

                done()
             })
        })


        function batchInsert(max_events) {

            it('inserting '+max_events+' events simultanously', function(done) {

                this.timeout(0)

                var doneCalled = false
                var callbacks = 0
                for(var i = 0; i < max_events; i++) {

                     mapi.put( "/events", exceptionEvent, function(err, result, httpResponse){

                        if(err) {
                            // console.error(err)
                            if(!doneCalled) {
                                doneCalled = true
                                if(err.message == "INTERNAL_ERROR")
                                    return done()

                                return done(err)
                            }
                            return
                        }

                        assert.equal(result,"ok")

                        callbacks++
                        if(callbacks >= max_events)
                           done()
                     })

                }
            })

        }

   })


}, 10000)

