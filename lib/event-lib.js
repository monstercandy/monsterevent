var lib = module.exports = function(knex, vali, app) {


    function eventlogQueryParams(d){
          return vali.async(d, {
               "limit": {default: 100, numericality:{onlyInteger: true, strict: true, lessThanOrEqualTo: 100}},
               "offset": {default: {value:0}, isInteger:true},
               "order": {inclusion: ["e_username","e_event_type","e_subsystem", "e_timestamp", "e_ip"], default: "e_timestamp"},
               "desc": {isBooleanLazy: true},

               "result_apply_timezone": {isString: true},

               // where fields
               "notbefore": {isString: true},
               "notafter": {isString: true},
               "auth_only": {isBooleanLazy: true},

               "e_event_type": {isString: true},
               "e_event_type_like": {isString: {trim:true}},
               "e_other": {isString: true},
               "e_username": {isString: true},
               "e_agent": {isString: true},
               "e_user_id": {isString: true},
               "e_ip": {isString: {trim:true}},
               "e_subsystem": {isString: {trim:true}},
          })
    }
    function eventlogQueryWhere(d, knex){

       var qStr = [1]
       var qArr = []

       if(d.notbefore){
          qStr.push("e_timestamp>=?")
          qArr.push(d.notbefore)
       }
       if(d.notafter){
          qStr.push("e_timestamp<=?")
          qArr.push(d.notafter)
       }
       if(d.auth_only){
          qStr.push(lib.SQL_FILTER_AUTH)
       }

       if(d.e_other){
          qStr.push("e_other LIKE ?")
          qArr.push('%'+d.e_other+'%')
       }
       if(d.e_username){
          qStr.push("e_username LIKE ?")
          qArr.push('%'+d.e_username+'%')
       }
       if(d.e_user_id){
          qStr.push("e_user_id=?")
          qArr.push(d.e_user_id)
       }
       if(d.e_ip){
          qStr.push("e_ip LIKE ?")
          qArr.push('%'+d.e_ip+'%')
       }
       if(d.e_agent){
          qStr.push("e_agent LIKE ?")
          qArr.push('%'+d.e_agent+'%')
       }
       if(d.e_event_type_like){
          qStr.push("e_event_type LIKE ?")
          qArr.push(d.e_event_type_like+'%')
       }
       if(d.e_event_type){
          qStr.push("e_event_type=?")
          qArr.push(d.e_event_type)
       }
       if(d.e_subsystem){
          qStr.push("e_subsystem=?")
          qArr.push(d.e_subsystem)
       }

       var qStr = qStr.join(" AND ")

       return knex.whereRaw(qStr, qArr)
    }


    const dotq = require("MonsterDotq");

    const dont_escalate = app.config.get("dont_escalate_exceptions");
    const exception_flush_seconds = app.config.get("exception_flush_seconds");
    const exception_flush_seconds_failure = app.config.get("exception_flush_seconds_failure");


    var re = {}


    const moment = require("MonsterMoment") // we require this so now will exist for sure
    const MError = app.MError


    var ssh_whitelister_ips = app.config.get("mail_subsystem_ssh_whitelister_emails");


    if(!app.eventSync)
       app.eventSync = [];
    if(!app.loginSync)
       app.loginSync = [];

    if(!app.banStats)
       app.banStats = {};

    if(!app.exceptionSync)
      app.exceptionSync = [];

    if(!app.exceptionSyncTimer)
       rescheduleExceptionSyncTimer();

    if(!app.eventSyncTimer)
       rescheduleEventSyncTimer();

    re.Query = function (pd) {
           var d
           return eventlogQueryParams(pd)
              .then(ad=>{
                 d = ad
                 var s = eventlogQueryWhere(d,knex.select()
                   .table('eventlog')
                   .limit(d.limit)
                   .offset(d.offset)
                   .orderBy(d.order, d.desc?"desc":undefined))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {

                moment.ApplyTimezoneOnArrayOfHashes(rows, ["e_ts"], d.result_apply_timezone)
                return Promise.resolve({"events": rows })

              })
    }

    re.Count = function (d) {
           return eventlogQueryParams(d)
              .then(d=>{
                 var s = eventlogQueryWhere(d, knex('eventlog').count("*"))
                 // console.log(s.toString())
                 return s
              })
              .then(function(rows) {
                return Promise.resolve({"count": rows[0]["count(*)"] })
              })
    }


    re.InsertByHash = function(idata, ts){

          var Token = require("Token")
          var authData;

          var e_other;
          var data;
          var authToInsert;

          return vali.async(idata, {
               "e_user_id": {isString: {trim:true}, default: {value:""}},
               "e_username": {isString: {trim:true}, default: {value:""}},
               "e_agent": {isString: {trim:true}, default: {value:""}},
               "e_other": {isObject: {acceptString: true, acceptArray: true, acceptScalar: true}, default: {value:{}}},
               "e_ip": {isString: {trim:true}, default: {value:""}},
               "e_subsystem": {presence: true, inclusion: lib.SupportedSubSystems},
               "e_event_type": {presence: true, inclusion: lib.SupportedEventTypes},
          }).then(function(adata){
               data = adata
               return Token.GetPseudoTokenAsync(data["e_username"]+" "+data["e_subsystem"], data["e_ip"]+" "+data["e_event_type"], ts, 8, "hex")
          }).then(function(uuid){
               data.e_id = uuid
               data.e_timestamp = moment.now()

               e_other = data.e_other

               // authentication based stuff


               if((data.e_event_type.match(/^auth-/))||(data.e_event_type.match(/switch-user/))){
                 authData = {};
                 Object.keys(data).forEach(oldKey=>{
                    var v = data[oldKey];
                    var newKey = oldKey.replace("e_", "l_");
                    authData[newKey] = v;
                 });

                 if(!e_other.successful)
                    addBanIp(data.e_ip, data.e_subsystem);


                 if((e_other.successful)&&
                    (data.e_subsystem == "email")&&
                    (ssh_whitelister_ips.indexOf(data.e_username) > -1))
                 {
                   console.log("login event detected for ssh whitelisting");
                   return app.GetRelayerMapiPool().postAsync("/s/[this]/iptables/hostssh", {
                      ip:data.e_ip, 
                      email: data.e_username
                    })
                    .catch(ex=>{
                       console.error("Could not change the iptables host ssh rule", ex)
                    })
                 }
               }



               const no_result=  {country:""}
               // return Promise.resolve(no_result)
               return app.MonsterInfoGeoip.GetInfo(data.e_ip)
                 .catch(ex=>{
                    console.error("Unable to lookup country", data, ex)
                    return Promise.resolve(no_result)
                 })

          })
          .then(countryLookup=>{
               data.e_country = countryLookup.country;
               if(authData)
                  authData.l_country = countryLookup.country;

               app.eventSync.push(data);
               // console.log("pushed to eventSync", app.eventSync.length)

               exceptionWorks(data);

               if(authData){
                 authData.l_successful = e_other.successful || false;
                 delete e_other.successful;
                 authData.l_other = JSON.stringify(e_other);

                 app.loginSync.push(authData);
               }



          })
          .then(()=>{
               return Promise.resolve("ok")
          })

    }

    if(!app.banReleaseLogic) {

      app.banReleaseLogic = true;
      setInterval(function(){
        var now = moment.nowUnixtime();
        var minSecondsWithoutFailure = app.config.get("ban_after_unsuccessful_attempts_in_seconds");
        Object.keys(app.banStats).forEach(ip=>{
            var x = app.banStats[ip];
            if(now - x.last > minSecondsWithoutFailure) {
              console.log("releasing ban stats for", ip);
              delete app.banStats[ip];
            }
        })

      }, app.config.get("ban_release_logic_interval_second")*1000)

    }


    return re;

    function addBanIp(ip, subsystem){
       if(!ip) return;
       if(!app.banStats[ip])
         app.banStats[ip] = {c: 0};
       var x = app.banStats[ip];
       x.c++;
       x.last = moment.nowUnixtime();

       var limit = app.config.get("ban_after_unsuccessful_attempts_count_"+subsystem) || app.config.get("ban_after_unsuccessful_attempts_count_all");

       if(x.c >= limit) {
          console.log("ip exceededed the limit, banning", ip, limit);
          var category = app.config.get("subsystem_to_ban_category")[subsystem];
          if(!category) {
             console.error("Unable to ban remote, no category mapping found for subsystem", subsystem)
             return;
          }

          return app.GetRelayerMapiPool().putAsync("/s/[this]/iptables/bans/"+category, {ip:ip})
            .catch(ex=>{
                console.error("Unable to ban IP", ip, ex);
            })
       }
    }

    function is_slowlog_due_to_network(lines){
        for(var line of lines) {
           for(var p of app.config.get("skip_slowlog_when_network_related")) {
              if(-1 < line.indexOf(p)) {
                 // console.log("skipping", line, p)
                 return true;
              }
           }
        }
        return false;
    }

    function exceptionWorks(data){
         if(data.e_event_type != "exception") return;
         if((!data.e_other)||(!data.e_other.message)) return;
         var type = data.e_other.message;
         if(dont_escalate.indexOf(type) > -1) return;

         if((type == "WEBHOSTING_SLOWLOG_EVENT")&&(data.e_other.slowQuery)&&(data.e_other.slowQuery.sl_dump)) {
             if(is_slowlog_due_to_network(data.e_other.slowQuery.sl_dump))
                return;
         }

         delete data.e_other.message;
         var n = extend({subsystem: data.e_subsystem, type: type}, data.e_other);
         if(data.e_user_id)
            n.e_user_id = data.e_user_id;

         app.exceptionSync.push(n);
    }


    function rescheduleExceptionSyncTimer(seconds){
        if(!seconds) seconds = exception_flush_seconds;
        var exc;
        app.exceptionSyncTimer = setTimeout(function(){
           return Promise.resolve()
            .then(()=>{
               exc = app.exceptionSync;
               app.exceptionSync = [];
               if(exc.length <= 0) return;

               return escalateEvents(exc);
            })
            .then(()=>{
               return rescheduleExceptionSyncTimer();
            })
            .catch(ex=>{
                console.error("Couldn't sync exceptions:", ex);
                return rescheduleExceptionSyncTimer(exception_flush_seconds_failure);
            })
        }, seconds * 1000);
    }

    function escalateEvents(events) {
        return app.GetRelayerMapiPool().putAsync("/accountapi/escalations", {events: events});
      }


    function rescheduleEventSyncTimer(){
        // console.log("scheduling this sthit")

        app.eventSyncTimer = setTimeout(function(){

            var eventToInsert = app.eventSync;
            app.eventSync = [];

            var loginToInsert = app.loginSync;
            app.loginSync = [];

            // console.log("doing shit", eventToInsert.length, loginToInsert.length);

            return Promise.resolve()
              .then(()=>{
                 if((!eventToInsert.length)&&(!loginToInsert.length)) return;

                 console.log("inserting ", eventToInsert.length, loginToInsert.length, "events");

                  return knex.transaction(trx=>{

                     return dotq.linearMap({array:eventToInsert,action:function(row){
                        var cdata = extend({}, row);
                        cdata.e_other = JSON.stringify(cdata.e_other);

                        return trx.into("eventlog").insert(cdata).return()
                     }})
                     .then(()=>{
                        return dotq.linearMap({array:loginToInsert,action:function(row){
                          return trx.into("loginlog").insert(row).return()
                       }})
                     })

                  })
                  .then(()=>{
                     console.log("Successfully inserted", eventToInsert.length, loginToInsert.length, "events");
                  })


              })
              .catch(ex=>{
                 // but the show must go on...
                 console.error("We were unable to flush events to loginlog", ex)
              })
              .then(()=>{
                 return rescheduleEventSyncTimer()
              })


        }, app.config.get("event_sync_flush_seconds")*1000)

    }
}

lib.SupportedSubSystems = [
  "accountapi", "tapi", "relayer", "mailer", "webhosting", "eventapi" , "docrootapi",
  "git", "dbms", "fileman", "ftp", "geoip", "email", "installatron", "certificate",
  "iptables",
]
lib.SupportedEventTypes = [
  "startup", "exception", "auth-credential", "auth-token", "auth-switch-user", "superuser-switch-user", "logout", "other",
  "registration", "email-notification", "email-verify", "email-send", "grants-update", "grants-delete", "grants-add",
  "account-insert", "account-change", "force-password","change-password",
  "account-grant-set", "account-grant-cleared",
  "account-domain-access-granted", "account-domain-access-revoked",
  "account-webhosting-access-granted", "account-webhosting-access-revoked",
  "callback-request", "email-send-direct",
  "email-confirmation-verify","email-confirmation-send", "email-insert",
  "forgotten-password-email-send",
  "email-removed", "email-primary-changed",
  "tel-verified", "tel-primary-changed", "tel-removed","tel-insert",
  "tel-code-sent",
  "token-deleted",
  "server-insert", "server-roles-changed", "server-changed",
  "webhosting-template-insert", "webhosting-template-change", "webhosting-template-removed",
  "sms-send",

  "totp-activate", "totp-deactivate", "totp-force-off",

  "data-import", "data-export", "data-updated",

  "dns-zone-insert", "dns-zone-removed",
  "dns-zone-Lock", "dns-zone-Unlock", "soa-contact-change",
  "dns-record-insert", "dns-record-removed",
  "dns-zone-regenerate", "dns-record-replaced",

  "docroot-regen",
  "awstats-regen", "awstats-processed", "awstats-built", "awstats-create", "awstats-delete",
  "vhost-bandwidth-limit-exceeded","vhost-bandwidth-limit-relaxed",
  "certificate-detach",
  "domain-removed", "redirects-rebuilt", "frames-rebuilt", "webhostings-rebuilt",
  "webhostings-rebuilt", "webhosting-remove", "maintane",
  "docroot-create", "docroot-remove", "docroot-tweak",
  "protected-dir-create","protected-dir-remove",
  "redirect-create","redirect-remove",
  "server-rehashed",
  "hook-changed",

  "git-change", "git-removed", "git-create", "branch-create","branch-removed","branch-op",

  "guixfer",

  "ban_release", "ban_insert",
  "firewall_status_change", "firewall_rule_insert", "firewall_rule_remove",
  "forbidden-http-request", "protected-http-request-ban",


  "virus-detected",
  "ftp-site-deleted", "ftp-site-inserted",
  "ftp-acl-ip-added", "ftp-acl-ip-removed", "ftp-acl-ips-changed",
  "ftp-site-public-changed", "ftp-site-admin-login-changed",
  "ftp-account-removed", "ftp-account-created",

  "readonly-readwrite", "wipe", "maildirmake","download","upload","untar","untgz","unzip",
  "delbackup","tar","tgz","remove","cleanup","move","copy","mkdir", "template",
  "cron-insert", "cron-removed", "restore-cron",

  "webhosting-create","webhosting-change",
  "szar-backup", "szar-restore",
  "hook-run",
  "php-fpm-config", "php-fpm-rehash","python-config","python-requirements","python-removed","python-insert",

  "git-create", "git-remove", "branch-create", "branch-remove" ,

  "restore-wh-docroot", "restore-wh-ftp", "restore-wh-iptables", "restore-wh-databases", "restore-wh-certs", "restore-wh-gits", "restore-wh-webstores", "restore-wh-emails",

  "database-create", "database-remove",
  "dbms-create","dbms-remove",
  "dbuser-create","dbuser-remove",
  "dbuser-toReadOnly","dbuser-ToReadWrite",
  "database-Import", "database-Export", "database-Repair",
  "dbms-store-stats",

  "iptables-hostssh-ip-set",

  "email-account-create", "email-alias-create", "email-bcc-create", "email-domain-create",
  "email-account-remove", "email-alias-remove", "email-bcc-remove", "email-domain-remove",
  "cleanup-junk", "recalculate-tallies", "quarantine-removed","amavis-cleanup",
  "cluebringer-policy-member-insert", "cluebringer-policy-member-removed",
  "cluebringer-quota-change",
  "cluebringer-policy-removed","cluebringer-policy-insert",
  "cluebringer-group-member-insert","cluebringer-group-member-removed",
  "whitelist-insert","whitelist-removed",
  "cleanup-old-emails",
  "spamassassin-learn",
  "sieve-relay",
  "greylisting-change",

  "restore-wh-docker", "upgrade-container", "docker-docrootapi", "shutdown-container",
  "kill-container", "restart-container", "stop-container", "start-container", "remove-container",
  "new-container", "runcmd-container", "execcmd-container", "raw-docker", "webstore-docker-config",
  "pull-image", "container-docker-config", "rebuild-containers", "upgrade-containers", "delete-image",
  "cleanup-images",

  "certificate-create", "activate", "revoke", "reissue",
  "certificate-change", "dispatch","renew","purchase-removed",
  "purchase-remove-expired",
]

