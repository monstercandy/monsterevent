FROM monstercommon

ADD lib /opt/MonsterEvent/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterEvent/lib/bin/event-install.sh && /opt/MonsterEvent/lib/bin/event-test.sh

ENTRYPOINT ["/opt/MonsterEvent/lib/bin/event-start.sh"]
