
exports.up = function(knex, Promise) {

    return Promise.all([


        knex.schema.createTable('eventlog', function(table){
            table.uuid('e_id');
            table.uuid('e_user_id').notNullable();
            table.uuid('e_username').notNullable();
            table.timestamp('e_timestamp').notNullable();
            table.string('e_ip').notNullable();
            table.string('e_subsystem').notNullable();
            table.string('e_event_type').notNullable();
            table.string('e_agent').notNullable().defaultTo("");
            table.boolean('e_successful').notNullable();
            table.string('e_other').notNullable().defaultTo("");
            table.boolean('e_processed').notNullable().defaultTo(0);       

            // todo: reverse dns, country/city

            table.index(["e_id"]);
            table.index(["e_processed","e_successful","e_user_id"]);
        }),


    ])
	
};

exports.down = function(knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('eventlog'),
    ])
	
};
